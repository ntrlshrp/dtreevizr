---
title: "Decision Trees"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Decision Trees}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
resource_files:
  - ./
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  echo = FALSE,
  message = FALSE,
  warning = FALSE
)
```

```{r}
library(magrittr)
library(dtreevizr)
```

```{r}
# data("boston_house_data")
# data("breast_cancer_data")
# data("census_adults_data")
```

> Decision trees are the fundamental building block of gradient boosting machines and Random Forests™, probably the two most popular machine learning models for structured data.^[https://explained.ai/decision-tree-viz/index.html]

I recently came across Terence Parr's [`dtreeviz`](https://libraries.io/pypi/dtreeviz).
This is a Python library for decision tree visualization.
I couldn't find `dtreeviz` in R, so I started writing a bare-bones package.

What's a decision tree?
As noted above, they are a fundamental building block in two popular machine learning models for structured data.
Structured data means we know the ground truth---these observations are apples and these observations are grapes.
It also means that we have some other features about our apples and grapes---weight, color, time of harvest, etc.
Essentially, a decision tree starts with all the data and asks, "What is the single cut on a single feature that creates two subgroups of data that are maximally similar within groups?"

> "For regression, similarity in a leaf means a low variance among target values and, for classification, it means that most or all targets are of a single class."^[https://explained.ai/decision-tree-viz/index.html]

For example, line up all of our apples and grapes by color and take those to the left of green (reds, oranges, and yellows) as apples, and those that are green or more blue as grapes.
This might do well, but we would probably have apples and grapes in both subgroups.

```{r, fig.cap="Toy data of apples and grapes, cut by color"}
set.seed(10204)
fruits <- data.frame(type = sample(c("apple", "grape"), 500, replace = TRUE),
                     color = sample(c("red", "orange", "yellow", "green", "blue", "indigo", "violet"), 50, replace = TRUE))

fruits$weight[fruits$type == "apple"] <-
  rnorm(sum(fruits$type == "apple"), 200, 40)
fruits$weight[fruits$type == "grape"] <-
  rnorm(sum(fruits$type == "grape"), 60, 12)

apple_tree_1 <- rpart::rpart(type ~ color, fruits)
dtreevizr_rpart_plot(dtreevizr_rpart(apple_tree_1), "fruits1")
```

If we cut by weight, however, we could likely put all the grapes in one group and all the apples in the other.

```{r, fig.cap="Toy data of apples and grapes, cut by weight"}
apple_tree_2 <- rpart::rpart(type ~ weight, fruits)
dtreevizr_rpart_plot(dtreevizr_rpart(apple_tree_2), "fruits2")
```

Controlling both the first cut and subsequent cuts, various rules (e.g., the size of the subgroups, the increased similarity of another cut, etc.) determine whether or not more cuts are made for more subgroups.

## Reproducing `dtreeviz` outputs

To demonstrate the package, `dtreevizr`, I have recreated some of [the examples shared](https://colab.research.google.com/github/parrt/dtreeviz/blob/master/notebooks/examples.ipynb) by the original `dtreeviz`.
First, you will see three visualizations from the `partykit`, `rpart.plot`, and `rattle` packages.
Then you will see the `dtreevizr` visualization.

#### Boston housing data (NT, NP)

Let's start with Boston housing data where we want to predict the median value of houses in a census tract.

```{r eval=FALSE, include=FALSE}
boston_house_data_link <- "https://archive.ics.uci.edu/ml/machine-learning-databases/housing/housing.data"
boston_house_names_link <- "https://archive.ics.uci.edu/ml/machine-learning-databases/housing/housing.names"
download.file(boston_house_data_link, "housing.data")
download.file(boston_house_names_link, "housing.names")

boston_house_data <- read.fwf("housing.data",
                              widths = c(8, 7, 8, 3, 8, 
                                         8, 7, 8, 4, 7,
                                         7, 7, 7, 7))
colnames(boston_house_data) <-
  c('CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD',
       'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV')



breast_cancer_data_link <- "https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.data"
breast_cancer_names_link <- "https://archive.ics.uci.edu/ml/machine-learning-databases/breast-cancer-wisconsin/wdbc.names"
download.file(breast_cancer_data_link, "cancer.data")
download.file(breast_cancer_names_link, "cancer.names")

breast_cancer_data <- read.csv("cancer.data", 
                               header = FALSE)
colnames(breast_cancer_data) <-
  c('ID', 'Diagnosis',
    'mean radius', 'mean texture', 'mean perimeter', 'mean area',
       'mean smoothness', 'mean compactness', 'mean concavity',
       'mean concave points', 'mean symmetry', 'mean fractal dimension',
       'radius error', 'texture error', 'perimeter error', 'area error',
       'smoothness error', 'compactness error', 'concavity error',
       'concave points error', 'symmetry error',
       'fractal dimension error', 'worst radius', 'worst texture',
       'worst perimeter', 'worst area', 'worst smoothness',
       'worst compactness', 'worst concavity', 'worst concave points',
       'worst symmetry', 'worst fractal dimension')
```

```{r, fig.cap="Recreating `dtreeviz` Boston housing visualization, numeric target, numeric predictors (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(MEDV ~ ., boston_house_data, 
                     control = rpart::rpart.control(maxdepth = 3))
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_1 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_1)
plot(partykit::as.party(prune_tree_1))
rpart.plot::prp(prune_tree_1)
rattle::fancyRpartPlot(prune_tree_1)	
```

```{r, fig.cap="Recreating `dtreeviz` Boston housing visualization, numeric target, numeric predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_1), "boston")
```

-----

#### Iris data (CT, NP)

For Iris data, we want to predict the plant species.

```{r, fig.cap="Recreating `dtreeviz` Iris visualization, categorical target, numeric predictors (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(Species ~ ., iris, 
                     control = rpart::rpart.control(maxdepth = 2))
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_2 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_2)
plot(partykit::as.party(prune_tree_2))
rpart.plot::prp(prune_tree_2)
rattle::fancyRpartPlot(prune_tree_2)
```

```{r, fig.cap="Recreating `dtreeviz` Iris visualization, categorical target, numeric predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_2), "iris")
```

-----

#### Breast cancer data (CT, NP)

For Breast cancer data, we want to predict the malignancy.

```{r, fig.cap="Recreating `dtreeviz` Boston housing visualization, categorical target, numeric predictors (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(Diagnosis ~ ., breast_cancer_data, 
                     control = rpart::rpart.control(maxdepth = 2))
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_6 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_6)
plot(partykit::as.party(prune_tree_6))
rpart.plot::prp(prune_tree_6)
rattle::fancyRpartPlot(prune_tree_6)	
```

```{r, fig.cap="Recreating `dtreeviz` breast cancer visualization, categorical target, numeric predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_6), "breast_cancer")
```

## Decision tree on a numeric target

### Decision tree on a numeric target, numeric predictors (NT, NP)

#### USArrests

For USArrests data, we want to predict the murder rate.

```{r, fig.cap="Decision tree on a numeric target, numeric predictors (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(Murder ~ ., USArrests, 
                     control = rpart::rpart.control(maxdepth = 7))
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_4 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_4)
plot(partykit::as.party(prune_tree_4))
rpart.plot::prp(prune_tree_4)
rattle::fancyRpartPlot(prune_tree_4)
```

```{r, fig.cap="Decision tree on a numeric target, numeric predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_4), "USArrests")
```

### Decision tree on a numeric target, categorical predictors (NT, CP)

- I haven't seen `dtreeviz` visualizations that use categorical predictors, this may be a new bonus of `dtreevizr` for R.

#### Iris data

For Iris data, we want to predict the petal length.

```{r, fig.cap="Decision tree on a numeric target, categorical predictor (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(Petal.Length ~ Species, iris)
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_9 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_9)
plot(partykit::as.party(prune_tree_9))
rpart.plot::prp(prune_tree_9)
rattle::fancyRpartPlot(prune_tree_9)
```

```{r, fig.cap="Decision tree on a numeric target, categorical predictor (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_9), "iris_cat_predictor")
```

### Decision tree on a numeric target, mixed predictors (NT, MP)

#### Census adults data

For Census adults data, we want to predict if income greater than $50k.
This crashes `partykit` so it is not shown below.

```{r eval=FALSE, include=FALSE}
census_adults_data_link <- "https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data"
census_adults_names_link <- "https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.names"
download.file(census_adults_data_link, "census_adults.data")
download.file(census_adults_names_link, "census_adults.names")

census_adults_data <- read.csv("census_adults.data", 
                               header = FALSE)
colnames(census_adults_data) <-
  c("age", 
"workclass", 
"fnlwgt", 
"education", 
"education-num", 
"marital-status", 
"occupation", 
"relationship", 
"race", 
"sex", 
"capital-gain", 
"capital-loss", 
"hours-per-week", 
"native-country",
"income")
```

```{r, fig.cap="Decision tree on a numeric target, mixed predictors (`rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(age ~ ., dplyr::select(census_adults_data, -c("marital-status")), weights = census_adults_data$fnlwgt, 
                     control = rpart::rpart.control(maxdepth = 7))
best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_12 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_12)
# plot(partykit::as.party(prune_tree_12)) # crashes R
rpart.plot::prp(prune_tree_12)
rattle::fancyRpartPlot(prune_tree_12)
```

```{r, fig.cap="Decision tree on a numeric target, mixed predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_12), "census_mixed_predictor")
```

## Decision tree on a categorical target

### Decision tree on a categorical target, numeric predictors (CT, NP)

#### Titanic data

For Titanic data, we want to predict survival.

```{r, fig.cap="Decision tree #2 on a categorical target, numeric predictors (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(Survived ~ ., Titanic)
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_7 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_7)
plot(partykit::as.party(prune_tree_7))
rpart.plot::prp(prune_tree_7)
rattle::fancyRpartPlot(prune_tree_7)
```

```{r, fig.cap="Decision tree #2 on a categorical target, numeric predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_7), "Titanic")
```

#### diamonds data

For diamonds data, we want to predict the cut of the diamond.

```{r, fig.cap="Decision tree #3 on a categorical target, numeric predictors (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
library('ggplot2')
tree <- rpart::rpart(cut ~ ., diamonds, 
                     control = rpart::rpart.control(maxdepth = 7))
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_5 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_5)
plot(partykit::as.party(prune_tree_5))
rpart.plot::prp(prune_tree_5)
rattle::fancyRpartPlot(prune_tree_5)
```

```{r, fig.cap="Decision tree on a categorical target, numeric predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_5), "diamonds")
```

### Decision tree on a categorical target, categorical predictors (CT, CP)

#### Census adults data

For Census adults data, we want to predict if income greater than $50k.

```{r, fig.cap="Decision tree on a categorical target, categorical predictors (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(income ~ race + sex + workclass + education, census_adults_data, weights = census_adults_data$fnlwgt)
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_13 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_13)
plot(partykit::as.party(prune_tree_13))
rpart.plot::prp(prune_tree_13)
rattle::fancyRpartPlot(prune_tree_13)
```

```{r, fig.cap="Decision tree on a categorical target, categorical predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_13), "census_adults_CTCP")
```


### Decision tree on a categorical target, mixed predictors (CT, MP)

#### Census adults data

For Census adults data, we want to predict if income greater than $50k.

```{r, fig.cap="Decision tree on a categorical target, mixed predictors (`partykit`, `rpart.plot`, and `rattle`)", fig.show='hold', out.width='33%'}
tree <- rpart::rpart(income ~ ., census_adults_data, weights = census_adults_data$fnlwgt)
# print(tree$cptable)
# rpart::plotcp(tree)

best_cp = tree$cptable[which.min(tree$cptable[ ,"xerror"]), "CP"]
prune_tree_8 <- rpart::prune(tree, 
                                cp = best_cp)

# plot(partykit::as.party(tree))
# partykit::as.party(prune_tree_8)
plot(partykit::as.party(prune_tree_8))
rpart.plot::prp(prune_tree_8)
rattle::fancyRpartPlot(prune_tree_8)
```

```{r, fig.cap="Decision tree on a categorical target, mixed predictors (`dtreevizr`)", echo=TRUE}
dtreevizr_rpart_plot(dtreevizr_rpart(prune_tree_8), "census_adults")
```
