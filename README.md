
# dtreevizr

<!-- badges: start -->
[![ Pipeline Status](https://gitlab.com/ntrlshrp/dtreevizr/badges/master/pipeline.svg)](https://gitlab.com/ntrlshrp/dtreevizr/-/commits/master)
[![CRAN status](https://www.r-pkg.org/badges/version/dtreevizr)](https://CRAN.R-project.org/package=dtreevizr)
[![Lifecycle: experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
<!-- badges: end -->

The goal of dtreevizr is to create beautiful visualizations of decision trees in the style of the Python library, [`dtreeviz`](https://libraries.io/pypi/dtreeviz).

## Installation

You can install the released version of `dtreevizr` from [CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("dtreevizr")
```

Or the development version from GitLab:

``` r
## install.packages("remotes")
remotes::install_gitlab("ntrlshrp/dtreevizr")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(dtreevizr)
tree <- rpart::rpart(Species ~ ., iris, 
                     control = rpart::rpart.control(maxdepth = 2))
dtreevizr_rpart_plot(dtreevizr_rpart(tree), "iris")
```

